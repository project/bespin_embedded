This module insterts the Bespin code editor into specified textareas.

IMPORTANT:
  The Administer Bespin Embedded permission can allow the permission holder
  to inject javascript directly into the page. Only grant to trusted administrators.

Installing Bespin:
  To install Bespin, you need to download the BespinEmbedded-DropIn-0.8.0.tar.gz file
  from http://ftp.mozilla.org/pub/mozilla.org/labs/bespin/Embedded/ and extract the contents
  into /sites/all/libraries/bespin. The path /sites/all/libraries/bespin/BespinEmbedded.js
  should now be valid.
  
  If you want to customize the Bespin install, you can use the BespinEmbedded tarball.